README

# ZUP Teste #

App para iOS criado como teste enviado pela ZUP Innovations para a emiolo.com.

### Funcionalidades ###

O App contém as seguintes funcionalidades:

* Cadastro de filme;
* Listagem dos filmes salvos;
* Acesso à API do IMDb (omdb);

### Cadastro de filme ###

![Simulator Screen Shot 4 de jan de 2016 17.09.50.png](https://bitbucket.org/repo/zdnb6L/images/2848374469-Simulator%20Screen%20Shot%204%20de%20jan%20de%202016%2017.09.50.png)

### Listagem dos filmes salvos ###

![Simulator Screen Shot 4 de jan de 2016 17.09.47.png](https://bitbucket.org/repo/zdnb6L/images/3594346849-Simulator%20Screen%20Shot%204%20de%20jan%20de%202016%2017.09.47.png)

### Acesso à API do IMDb (omdb) ###

![Simulator Screen Shot 4 de jan de 2016 17.09.56.png](https://bitbucket.org/repo/zdnb6L/images/1555953609-Simulator%20Screen%20Shot%204%20de%20jan%20de%202016%2017.09.56.png)

# Desenvolvido por #

### Gabriel Barbosa ###
gabriel.barbosa@emiolo.com