//
//  GetJSON.swift
//  BomBem
//
//  Created by Gabriel Barbosa on 24/07/15.
//  Copyright (c) 2015 eMiolo. All rights reserved.
//

import Foundation

@objc protocol GetJSONDelegate{
    optional func didReturnMessage(parsedObject: NSDictionary, data: NSMutableData)
}

class GetJSON : NSObject, NSURLConnectionDelegate {
    
    var dados: NSMutableData = NSMutableData()
    var parsedObject: NSDictionary = NSDictionary()
    var delegate:GetJSONDelegate?
    
    init (urlPath: String) {
        
        super.init()
        
        let request: NSURLRequest = NSURLRequest(URL: NSURL(string: urlPath)!)
        let connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)!
        connection.start()
        
    }
    
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        self.dados = NSMutableData()
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!){
        self.dados.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        
        //var err: NSError?
        
        let retorno = try! NSJSONSerialization.JSONObjectWithData(self.dados,
            options: NSJSONReadingOptions.AllowFragments) as? NSDictionary
        
        if retorno != nil {
            parsedObject = retorno!
        }
        
        self.delegate?.didReturnMessage!(parsedObject, data: self.dados)
        
    }
    
    
}

