//
//  FilmInfoViewController.swift
//  ZUPTeste
//
//  Created by Gabriel Barbosa on 04/01/16.
//  Copyright © 2016 eMiolo. All rights reserved.
//

import UIKit
import CoreData

class FilmInfoViewController: UIViewController, UITableViewDelegate, SendJSONDelegate {
    var film: Film!
    var actors = [String]()
    
    @IBOutlet weak var castTable: UITableView?
    @IBOutlet weak var filmTitle: UILabel?
    @IBOutlet weak var filmReleased: UILabel?
    @IBOutlet weak var filmRate: UILabel?
    @IBOutlet weak var filmProduction: UILabel?
    @IBOutlet weak var filmDirector: UILabel?
    @IBOutlet weak var filmBoxOffice: UILabel?
    @IBOutlet weak var filmimdbRating: UILabel?
    @IBOutlet weak var filmPlot: UITextView?
    @IBOutlet weak var filmPoster: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layoutTable()
        loadFilmData()
    }
    
    func layoutTable() {
        castTable?.delegate = self
        castTable?.separatorInset = UIEdgeInsetsZero
        self.castTable?.layoutMargins = UIEdgeInsetsZero;
        
        let footer : UIView = UIView()
        footer.frame = CGRectZero
        footer.backgroundColor = UIColor.clearColor()
        self.castTable?.tableFooterView = footer
    }
    
    func loadFilmData() {
        filmTitle?.text = film.title!
        filmReleased?.text = film.released!
        filmRate?.text = "\(film.rated!)"
        filmimdbRating?.text = "\(film.imdbrating!)"
        filmProduction?.text = "Estúdio: \(film.production!)"
        filmDirector?.text = "Diretor: \(film.diretor!)"
        filmBoxOffice?.text = "Bilheteria: \(film.boxoffice!)"
        filmPlot?.text = film.plot!
        
        if let url = NSURL(string: film.poster!) {
            if let data = NSData(contentsOfURL: url){
                filmPoster?.image = UIImage(data: data)
            }
        }
        
        actors = film.actors!.componentsSeparatedByString(", ")
        
        self.castTable?.reloadData()
    }
    
    // ***************************
    // UITableViewDelegate Methods
    // ***************************
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actors.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier = "CastCell"
        
        //Cria célula do tipo ListFilmsCellController
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier)! as UITableViewCell
        
        cell.textLabel?.text = actors[indexPath.row]
        
        cell.layoutMargins = UIEdgeInsetsZero
        cell.separatorInset = UIEdgeInsetsZero
        
        return cell
        
    }
    

    
    //********************************************************
    //********************************************************
    
}
