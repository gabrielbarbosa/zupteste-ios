//
//  ListFilmsCellController.swift
//  ZUPTeste
//
//  Created by Gabriel Barbosa on 04/01/16.
//  Copyright © 2016 eMiolo. All rights reserved.
//

import UIKit

class ListFilmsCellController: UITableViewCell {
    
    var imageFilm: UIImage? {
        didSet {
            self.imageFilmCell.image = imageFilm
        }
    }
    var titleFilm: String? {
        didSet {
            self.titleFilmCell.text = titleFilm
        }
    }
    var releasedFilm: String? {
        didSet {
            self.releasedFilmCell.text = releasedFilm
        }
    }
    var directorFilm: String? {
        didSet {
            self.directorFilmCell.text = directorFilm
        }
    }
    
    @IBOutlet var imageFilmCell: UIImageView!
    @IBOutlet var titleFilmCell: UILabel!
    @IBOutlet var releasedFilmCell: UILabel!
    @IBOutlet var directorFilmCell: UILabel!
    
}

