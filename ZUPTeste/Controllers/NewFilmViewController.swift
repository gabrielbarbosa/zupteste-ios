//
//  NewFilmViewController.swift
//  ZUPTeste
//
//  Created by Gabriel Barbosa on 04/01/16.
//  Copyright © 2016 eMiolo. All rights reserved.
//

import UIKit
import CoreData

class NewFilmViewController: UIViewController, SendJSONDelegate, UITextFieldDelegate {
    
    var sendJSON: SendJSON?
    var carregando: LoadingViewController!
    
    @IBOutlet var filmTitle: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        let tapscreen : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:"dismissKeyboard:")
        self.view.addGestureRecognizer(tapscreen)
        
        filmTitle.delegate = self

    }
    
    func saveFilm(dict: NSDictionary) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //criando Filme no banco
        let entity =  NSEntityDescription.entityForName("Film", inManagedObjectContext:managedContext)
        let newFilm = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext) as! Film
        
        newFilm.fillObjectWithDictionary(dict)
        
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        let alert = UIAlertController(title: "ZUP Teste", message: "Filme adicionado com sucesso!", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) in
            self.carregando.stopLoading()
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func addFilm() {
        self.carregando = LoadingViewController(container: self.view)
        
        NSOperationQueue.mainQueue().addOperationWithBlock {
        
            let postMensagem : NSMutableArray? = NSMutableArray()
            self.sendJSON = SendJSON()
            self.sendJSON!.delegate = self
            self.sendJSON!.parametros = postMensagem!
            self.sendJSON!.url = "\(Variaveis.baseURL)t=\(self.filmTitle!.text!)&y=&plot=full&r=json&tomatoes=true"
            self.sendJSON!.preparaJSON("")
        }
    }
    
    func dismissKeyboard(recognizer:UITapGestureRecognizer) {
        filmTitle!.resignFirstResponder()
    }

    // ***************************
    // SendJSONDelegate Methods
    // ***************************
    func didReturnSuccessMessage(sucesso: Bool) {
        if (sucesso == true && sendJSON!.json != nil) {
            let dict = sendJSON!.json! as NSDictionary
            
             NSOperationQueue.mainQueue().addOperationWithBlock {
                self.saveFilm(dict)
            }
        }
        else {
            let alert = UIAlertController(title: "ZUP Teste", message: "Erro. Por favor, tente novamente", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) in
                self.carregando.stopLoading()
            }))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    //********************************************************
    //********************************************************
}
