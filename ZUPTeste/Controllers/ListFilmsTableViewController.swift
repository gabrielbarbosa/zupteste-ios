//
//  ListFilmsTableViewController.swift
//  ZUPTeste
//
//  Created by Gabriel Barbosa on 04/01/16.
//  Copyright © 2016 eMiolo. All rights reserved.
//

import UIKit
import CoreData

class ListFilmsTableViewController: UITableViewController, SendJSONDelegate {
    var sendJSON: SendJSON?
    var carregando: LoadingViewController!
    var films = [Film]()
    var selectedFilm: Film!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        let newFilm = UIBarButtonItem(title: "Novo", style: .Plain, target: self, action: Selector("newFilm"))
        self.navigationItem.rightBarButtonItem = newFilm
    }
    
    override func viewWillAppear(animated: Bool) {
        layoutTable()
        loadFilms()
    }
    
    func newFilm() {
        performSegueWithIdentifier("NewFilmSegue", sender: nil)
    }
    
    func loadFilms() {
        
        self.carregando = LoadingViewController(container: self.view)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        let fetchFilms = NSFetchRequest(entityName: "Film")
        
        do {
            let results = try managedContext.executeFetchRequest(fetchFilms)
            films = results as! [Film]
            self.tableView.reloadData()
            self.carregando.stopLoading()
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func layoutTable() {
        self.tableView.separatorInset = UIEdgeInsetsZero
        self.tableView.layoutMargins = UIEdgeInsetsZero;
        
        let footer : UIView = UIView()
        footer.frame = CGRectZero
        footer.backgroundColor = UIColor.clearColor()
        self.tableView.tableFooterView = footer
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "FilmInfoSegue" {
            let filmInfoViewController: FilmInfoViewController! = segue.destinationViewController as! FilmInfoViewController
            
            filmInfoViewController.film = selectedFilm
        }
    }
    
    // ***************************
    // UITableViewDelegate Methods
    // ***************************
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return films.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier = "FilmCell"
        
        //Cria célula do tipo ListFilmsCellController
        let cell: ListFilmsCellController! = tableView.dequeueReusableCellWithIdentifier(identifier) as? ListFilmsCellController
        
        let film = films[indexPath.row] as Film
        
        cell.titleFilm = film.title!
        cell.releasedFilm = "Lançamento: \(film.released!)"
        cell.directorFilm = "Diretor: \(film.diretor!)"
        
        if let url = NSURL(string: film.poster!) {
            if let data = NSData(contentsOfURL: url){
                cell.imageFilm = UIImage(data: data)
            }
        }
        
        cell.layoutMargins = UIEdgeInsetsZero
        cell.separatorInset = UIEdgeInsetsZero
        
        return cell
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedFilm = films[indexPath.row] as Film
        performSegueWithIdentifier("FilmInfoSegue", sender: nil)
    }

    //********************************************************
    //********************************************************
}