//
//  SendJSON.swift
//  BomBem
//
//  Created by Gabriel Barbosa on 22/07/15.
//  Copyright (c) 2015 eMiolo. All rights reserved.
//

import Foundation

@objc protocol SendJSONDelegate{
    optional func didReturnSuccessMessage(sucesso: Bool)
}

class SendJSON : NSObject, NSURLConnectionDelegate {
    
    var delegate: SendJSONDelegate?
    var sucesso: Bool = false
    var parametros: NSMutableArray? = NSMutableArray()
    var url: String?
    var json: NSDictionary!
    var jsonArray: NSMutableArray!
    
    override init () {
        
        super.init()
        
        
    }
    
    func preparaJSON(bodyData: String) {
        let request = NSMutableURLRequest(URL: NSURL(string: self.url!)!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "GET"
        
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding)
        
        var strData = NSString()
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if data == nil {
                self.sucesso = false
                self.delegate?.didReturnSuccessMessage!(self.sucesso)
            }
            else {
                strData = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                print("Body: \(strData)")
                if strData != "" {
                    self.json = try! NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as! NSDictionary
                    
                    if strData == "erro" {
                        self.sucesso = false
                    }
                    else {
                        self.sucesso = true
                    }
                }
                else {
                    self.sucesso = false
                }
                self.delegate?.didReturnSuccessMessage!(self.sucesso)
            }
        })
        
        task.resume()
        
    }
    
    func preparaJSONArray(bodyData: String) {
        let request = NSMutableURLRequest(URL: NSURL(string: self.url!)!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        //var params:NSMutableArray = respostas
        
        //var err: NSError?
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding)
        //request.HTTPBody = NSJSONSerialization.dataWithJSONObject(self.parametros!, options: NSJSONWritingOptions.PrettyPrinted, error: &err)
        //request.addValue("multipart/form-data; boundary=---------------------------14737809831466499882746641449", forHTTPHeaderField: "Content-Type")
        
        //request.addValue("image/jpeg", forHTTPHeaderField: "Content-Type")
        //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        var strData = NSString()
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            //println("Response: \(response)")
            strData = NSString(data: data!, encoding: NSUTF8StringEncoding)!
            print("Body: \(strData)")
            if strData != "" {
                //var err: NSError?
                self.jsonArray = try! NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as! NSMutableArray
                
                if strData == "erro" {
                    self.sucesso = false
                }
                else {
                    self.sucesso = true
                }
            }
            else {
                self.sucesso = false
            }
            self.delegate?.didReturnSuccessMessage!(self.sucesso)
        })
        
        task.resume()
        
    }
    
    func preparaJSONWithImage() {
        let request = NSMutableURLRequest(URL: NSURL(string: self.url!)!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        //var params:NSMutableArray = respostas
        
        //var err: NSError?
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(self.parametros!, options: NSJSONWritingOptions.PrettyPrinted)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        var strData = NSString()
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            //println("Response: \(response)")
            strData = NSString(data: data!, encoding: NSUTF8StringEncoding)!
            print("Body: \(strData)")
            //var err: NSError?
            self.json = try! NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as? NSDictionary
            
            if strData == "erro" {
                self.sucesso = false
            }
            else {
                self.sucesso = true
            }
            self.delegate?.didReturnSuccessMessage!(self.sucesso)
        })
        
        task.resume()
        
    }
    
}
