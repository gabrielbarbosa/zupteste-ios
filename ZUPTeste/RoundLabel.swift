//
//  RoundLabel.swift
//  ZUPTeste
//
//  Created by Gabriel Barbosa on 04/01/16.
//  Copyright © 2016 eMiolo. All rights reserved.
//

import UIKit

class RoundLabel: UILabel {
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder);
        
        self.layer.cornerRadius = 6.0
        self.clipsToBounds = true
        
    }
}
