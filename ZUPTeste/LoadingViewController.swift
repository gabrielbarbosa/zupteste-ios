//
//  LoadingViewController.swift
//  BomBem
//
//  Created by Gabriel Barbosa on 22/07/15.
//  Copyright (c) 2015 eMiolo. All rights reserved.
//

import UIKit

class LoadingViewController : UIView {
    
    init(container: UIView) {
        super.init(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 200, height: 200)))
        let telas = NSBundle.mainBundle().loadNibNamed("LoadingView", owner: self, options: nil) as NSArray
        
        let telaCarregando = telas.objectAtIndex(0) as! UIView
        
        telaCarregando.center = container.center
        
        telaCarregando.layer.cornerRadius = 6.0
        telaCarregando.clipsToBounds = true
        
        self.addSubview(telaCarregando)
        container.addSubview(self)
    }
    
    func stopLoading() {
        self.removeFromSuperview()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
