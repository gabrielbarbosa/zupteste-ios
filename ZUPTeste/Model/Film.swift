//
//  Film.swift
//  ZUPTeste
//
//  Created by Gabriel Barbosa on 04/01/16.
//  Copyright © 2016 eMiolo. All rights reserved.
//

import Foundation
import CoreData


class Film: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    func fillObjectWithDictionary(dict: NSDictionary) {
        
        if let titleTemp = dict["Title"] as? NSString {
            title = titleTemp as String
        }
        
        if let yearTemp = dict["Year"] as? NSString {
            year = yearTemp.integerValue
        }
        
        if let ratedTemp = dict["Rated"] as? NSString {
            rated = ratedTemp as String
        }
        
        if let releasedTemp = dict["Released"] as? NSString {
            released = releasedTemp as String
        }
        
        if let runtimeTemp = dict["Runtime"] as? NSString {
            runtime = runtimeTemp as String
        }
        
        if let genreTemp = dict["Genre"] as? NSString {
            genre = genreTemp as String
        }
        
        if let diretorTemp = dict["Director"] as? NSString {
            diretor = diretorTemp as String
        }
        
        if let writerTemp = dict["Writer"] as? NSString {
            writer = writerTemp as String
        }
        
        if let actorsTemp = dict["Actors"] as? NSString {
            actors = actorsTemp as String
        }
        
        if let plotTemp = dict["Plot"] as? NSString {
            plot = plotTemp as String
        }
        
        if let languageTemp = dict["Language"] as? NSString {
            language = languageTemp as String
        }
        
        if let countryTemp = dict["Country"] as? NSString {
            country = countryTemp as String
        }
        
        if let awardsTemp = dict["Awards"] as? NSString {
            awards = awardsTemp as String
        }
        
        if let posterTemp = dict["Poster"] as? NSString {
            poster = posterTemp as String
        }
        
        if let imdbratingTemp = dict["imdbRating"] as? NSString {
            imdbrating = imdbratingTemp.doubleValue
        }
        
        if let boxofficeTemp = dict["BoxOffice"] as? NSString {
            boxoffice = boxofficeTemp as String
        }
        
        if let productionTemp = dict["Production"] as? NSString {
            production = productionTemp as String
        }
        
        if let websiteTemp = dict["Website"] as? NSString {
            website = websiteTemp as String
        }
    }

}
