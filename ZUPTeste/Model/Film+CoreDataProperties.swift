//
//  Film+CoreDataProperties.swift
//  ZUPTeste
//
//  Created by Gabriel Barbosa on 04/01/16.
//  Copyright © 2016 eMiolo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Film {

    @NSManaged var title: String?
    @NSManaged var year: NSNumber?
    @NSManaged var rated: String?
    @NSManaged var released: String?
    @NSManaged var runtime: String?
    @NSManaged var genre: String?
    @NSManaged var diretor: String?
    @NSManaged var writer: String?
    @NSManaged var actors: String?
    @NSManaged var plot: String?
    @NSManaged var language: String?
    @NSManaged var country: String?
    @NSManaged var awards: String?
    @NSManaged var poster: String?
    @NSManaged var imdbrating: NSNumber?
    @NSManaged var boxoffice: String?
    @NSManaged var production: String?
    @NSManaged var website: String?

}
