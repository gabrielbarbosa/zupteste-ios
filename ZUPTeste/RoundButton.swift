//
//  RoundButton.swift
//  pracomprar
//
//  Created by Gabriel Barbosa on 09/09/15.
//  Copyright © 2015 eMiolo Soluções Internet LTDA. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder);
        
        self.layer.cornerRadius = 6.0
        self.clipsToBounds = true
        
    }
}
